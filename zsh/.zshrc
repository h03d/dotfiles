# Fish-like autosuggestions feature.
if [[ -f /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh ]]; then
   . /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
fi

if [[ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]]; then
    . /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

export TERM='xterm-256color'

# History Configuration
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

bindkey -e
bindkey '^[[3~' delete-char

# THEME
source /usr/share/zsh-theme-powerlevel9k/powerlevel9k.zsh-theme
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(ssh root_indicator context dir vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status background_jobs command_execution_time history)
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_RPROMPT_ON_NEWLINE=true
POWERLINE9K_MODE='awesome-patched'
POWERLEVEL9K_SHORTEN_DIR_LENGTH=3
POWERLEVEL9K_SHORTEN_STRATEGY="truncate_middle"
POWERLEVEL9K_STATUS_VERBOSE=false
# POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="╰`tty`─▶ "
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="╰─▶ "
POWERLEVEL9K_DIR_SHOW_WRITABLE=true

POWERLEVEL9K_SHOW_CHANGESET=true
POWERLEVEL9K_CHANGESET_HASH_LENGTH=6

# source /usr/share/doc/pkgfile/command-not-found.zsh

autoload -Uz run-help
unalias run-help
alias help=run-help

alias ls="ls --color"
alias diff="diff --color=auto"
GREP_COLORS=auto

# Powerline
if [[ -f /usr/share/powerline/bindings/zsh/powerline.zsh ]]; then
    powerline-daemon -q
    POWERLINE_BASH_CONTINUATION=1
    POWERLINE_BASH_SELECT=1
    . /usr/share/powerline/bindings/zsh/powerline.zsh
fi

# Autocompletion
zstyle ':completion:*' rehash true
zstyle ':completion:*' menu select
zstyle :compinstall filename '~/.zshrc'

setopt COMPLETE_ALIASES

autoload -Uz compinit
compinit

unsetopt NOMATCH
setopt NOTIFY
